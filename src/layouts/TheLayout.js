import React from 'react'
import {
  TheContent,
  TheSidebar,
  // TheFooter,
  // TheHeader
} from './index'
import styles from './styles.module.scss';

const TheLayout = () => {
  return (
    <div className={styles.c_app}>
      <TheSidebar />
      <div className={styles.c_wrapper}>
        {/* <TheHeader /> */}
        <div className={styles.c_body}>
          <TheContent />
        </div>
        {/* <TheFooter /> */}
      </div>
    </div>
  )
}

export default TheLayout
