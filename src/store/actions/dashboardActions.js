import axios from 'axios'

const index = () => {
  return dispatch => {
    axios.get()
      .then(res => {
        dispatch(add_data_index(res.data))
      })
  };
};

const show = (id) => {
  return dispatch => {
    // axios.get(`${}/${id}`)
    //   .then(res => {
    //     dispatch(add_show(res.data))
    //   })
  };
};

const destroy = (id) => {
  return dispatch => {
    // axios.delete(`${}/${id}`)
    //   .then(res => {
    //     dispatch(modal_destroy(false))
    //     dispatch(index())
    //   })
  };
};

const add_data_index = (values) => { return { type: 'ADD_INDEX', values } }
const add_show = (values) => { return { type: 'ADD_SHOW', values } }
const edit_mode = (values) => { return { type: 'EDIT_MODE', values } }
const modal_mode = (values) => { return { type: 'MODAL_MODE', values } }
// const modal_destroy = (values) => { return { type: 'MODAL_DESTROY', values } }

export default {
  index,
  show,
  destroy,
  add_data_index,
  add_show,
  edit_mode,
  modal_mode,
  // modal_destroy,
}