export const initialState = {
  index: null,
  show: null,
  edit_mode: false,
  modal_mode: false,
}

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_INDEX' : return {...state, index: action.values };
    case 'ADD_SHOW': return {...state, show: action.values };
    case 'EDIT_MODE': return {...state, edit_mode: action.values};
    case 'MODAL_MODE': return {...state, modal_mode: action.values};
    default: return state
  }
}

export default dashboardReducer;