import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress'

const loading = (<CircularProgress color="secondary" />)

const TheLayout = lazy(() => import('./layouts/TheLayout'))

const App = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={loading}>
        <Switch>
          {/* <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>}/>
          <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>}/> */}
          <Route path="/" name="Home" render = { props => <TheLayout {...props} /> } />
        </Switch>
      </Suspense>
    </BrowserRouter> 
  )
}

export default App;
